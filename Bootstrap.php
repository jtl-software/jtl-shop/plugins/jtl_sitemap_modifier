<?php declare(strict_types=1);

namespace Plugin\jtl_sitemap_modifier;

use JTL\Customer\CustomerGroup;
use JTL\Events\Dispatcher;
use JTL\Language\LanguageHelper;
use JTL\Plugin\Bootstrapper;
use JTL\Sitemap\Export;
use JTL\Sitemap\Factories\Product;
use Shop;

/**
 * Class Bootstrap
 * @package Plugin\jtl_sitemap_modifier
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @var bool
     */
    private $customExportDone = false;

    /**
     * @inheritDoc
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);
        $dispatcher->listen('shop.hook.' . \HOOK_SITEMAP_EXPORT_GENERATED, function () {
            if ($this->customExportDone === true) {
                return;
            }
            $this->customExportDone = true;
            $config                 = Shop::getSettings([\CONF_GLOBAL, \CONF_SITEMAP]);
            $exporter               = new Export(
                $this->getDB(),
                Shop::Container()->getLogService(),
                new ImageSitemapItemRenderer(),
                new ImageSchemaRenderer(),
                $config
            );
            $exporter->setFileName('image_sitemap_');
            $exporter->setIndexFileName(null);
            $exporter->generate(
                [CustomerGroup::getDefaultGroupID()],
                LanguageHelper::getAllLanguages(),
                [new ImageFactory(
                    $this->getDB(),
                    $config,
                    Shop::getURL() . '/',
                    Shop::getImageBaseURL()
                )]
            );
        });
        $dispatcher->listen('shop.hook.' . \HOOK_SITEMAP_EXPORT_GENERATE, function ($args) {
            $filteredProductFactory = new FilteredProductFactory(
                $this->getDB(),
                Shop::getSettings([\CONF_GLOBAL, \CONF_SITEMAP]),
                Shop::getURL() . '/',
                Shop::getImageBaseURL()
            );
            /** @var array $factories */
            $factories = &$args['factories'];
            foreach ($factories as $i => $factory) {
                if (\get_class($factory) === Product::class) {
                    unset($factories[$i]);
                    $factories[] = $filteredProductFactory;
                    break;
                }
            }
        });
    }
}
