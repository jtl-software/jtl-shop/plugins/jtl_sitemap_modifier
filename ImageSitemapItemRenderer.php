<?php declare(strict_types=1);
namespace Plugin\jtl_sitemap_modifier;

use JTL\Sitemap\ItemRenderers\AbstractItemRenderer;
use JTL\Sitemap\Items\ItemInterface;

/**
 * Class ImageSitemapItemRenderer
 * @package Plugin\jtl_sitemap_modifier
 */
class ImageSitemapItemRenderer extends AbstractItemRenderer
{
    /**
     * @inheritDoc
     */
    public function renderItem(ItemInterface $item): string
    {
        /** @var ImageSitemapItem $item */

        $xml = "<url>\n" .
            '    <loc>' . $item->getLocation() . "</loc>\n";
        foreach ($item->getImages() as $image) {
            $xml    .= "    <image:image>\n";
            $xml    .= '        <image:loc>' . $image->getLocation() . "</image:loc>\n";
            $title   = $image->getTitle();
            $caption = $image->getCaption();
            $license = $image->getLicense();
            if (!empty($title)) {
                $xml .= '        <image:title>' . $title . "</image:title>\n";
            }
            if (!empty($caption)) {
                $xml .= '        <image:caption>' . $caption . "</image:caption>\n";
            }
            if (!empty($license)) {
                $xml .= '        <image:license>' . $license . "</image:license>\n";
            }
            $xml .= "    </image:image>\n";
        }

        return $xml . "  </url>\n";
    }
}
