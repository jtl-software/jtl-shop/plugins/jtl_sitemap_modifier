## Sitemap Modifier

Dieses Plugin zeigt, wie im JTL-Shop 5.0.0+ bestehende Sitemaps modifiziert werden und außerdem eigene Sitemaps erstellt werden können.

Es beschränkt zur Anschauung die normalen Produkte auf diejenigen, deren ID ohne Rest durch 3 geteilt werden kann.

Darüber hinaus erstellt es eine neue Sitemap mit dem Namen image_sitemap_0.xml ohne extra Index-Datei nach dem Schema für [Google Image Sitemaps](https://support.google.com/webmasters/answer/178636).   
