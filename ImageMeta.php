<?php declare(strict_types=1);
namespace Plugin\jtl_sitemap_modifier;

/**
 * Class ImageMeta
 * @package Plugin\jtl_sitemap_modifier
 */
class ImageMeta
{
    /**
     * @var string|null
     */
    private $license;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $caption;

    /**
     * @var string
     */
    private $location;

    /**
     * @return string|null
     */
    public function getLicense(): ?string
    {
        return $this->license;
    }

    /**
     * @param string|null $license
     */
    public function setLicense(?string $license): void
    {
        $this->license = $license;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param string|null $caption
     */
    public function setCaption(?string $caption): void
    {
        $this->caption = $caption;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }
}
