<?php declare(strict_types=1);
namespace Plugin\jtl_sitemap_modifier;

use JTL\Catalog\Product\Artikel;
use JTL\Sitemap\Items\AbstractItem;

/**
 * Class ImageSitemapItem
 * @package Plugin\jtl_sitemap_modifier
 */
class ImageSitemapItem extends AbstractItem
{
    /**
     * @var ImageMeta[]
     */
    protected $images = [];

    /**
     * @var string
     */
    protected $location;

    /**
     * @inheritDoc
     */
    public function generateData($data, array $languages): void
    {
        /** @var Artikel $data */
        foreach ($data->getImages() as $image) {
            $this->location = $data->cURLFull;
            $item = new ImageMeta();
            $item->setLocation($image->cURLGross);
            $item->setLicense('GNU Public License');
            $item->setTitle($data->cName);
            $this->images[] = $item;
        }
    }

    /**
     * @return ImageMeta[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param ImageMeta[] $images
     */
    public function setImages(array $images): void
    {
        $this->images = $images;
    }
}
