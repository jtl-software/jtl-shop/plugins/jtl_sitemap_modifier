<?php declare(strict_types=1);
namespace Plugin\jtl_sitemap_modifier;

use Generator;
use JTL\DB\ReturnType;
use JTL\Language\LanguageHelper;
use JTL\Sitemap\Factories\AbstractFactory;
use JTL\Sitemap\Items\Product as Item;
use PDO;
use function Functional\first;
use function Functional\map;

/**
 * Class Product
 * @package Plugin\jtl_sitemap_modifier
 */
final class FilteredProductFactory extends AbstractFactory
{
    /**
     * @inheritdoc
     */
    public function getCollection(array $languages, array $customerGroups): Generator
    {
        $defaultCustomerGroupID  = first($customerGroups);
        $defaultLang             = LanguageHelper::getDefaultLanguage();
        $defaultLangID           = (int)$defaultLang->kSprache;
        $_SESSION['kSprache']    = $defaultLangID;
        $_SESSION['cISOSprache'] = $defaultLang->cISO;
        $languageIDs             = map($languages, static function ($e) {
            return (int)$e->kSprache;
        });
        $res                     = $this->db->queryPrepared(
            "SELECT tartikel.kArtikel, tartikel.dLetzteAktualisierung AS dlm,  tseo.cSeo, tseo.kSprache AS langID
                FROM tartikel
                JOIN tseo 
                    ON tseo.cKey = 'kArtikel'
                    AND tseo.kKey = tartikel.kArtikel
                    AND tseo.kSprache IN (" . \implode(',', $languageIDs) . ')
                LEFT JOIN tartikelsichtbarkeit 
                    ON tartikel.kArtikel = tartikelsichtbarkeit.kArtikel
                    AND tartikelsichtbarkeit.kKundengruppe = :cgid
                WHERE tartikelsichtbarkeit.kArtikel IS NULL
                    AND tartikel.kVaterArtikel = 0
                    AND tartikel.kArtikel % 5 = 0
                ORDER BY tartikel.kArtikel',
            ['cgid' => $defaultCustomerGroupID],
            ReturnType::QUERYSINGLE
        );

        while (($product = $res->fetch(PDO::FETCH_OBJ)) !== false) {
            $item = new Item($this->config, $this->baseURL, $this->baseImageURL);
            $item->generateData($product, $languages);
            yield $item;
        }
    }
}
