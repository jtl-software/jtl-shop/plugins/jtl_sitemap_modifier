<?php declare(strict_types=1);
namespace Plugin\jtl_sitemap_modifier;

use JTL\Sitemap\SchemaRenderers\AbstractSchemaRenderer;

/**
 * Class ImageSchemaRenderer
 * @package Plugin\jtl_sitemap_modifier
 */
final class ImageSchemaRenderer extends AbstractSchemaRenderer
{
    /**
     * @param string[] $sitemapFiles
     * @return string
     */
    public function buildIndex(array $sitemapFiles): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function buildHeader(): string
    {
        return $this->getXmlHeader()
            . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">' . "\n";
    }

    /**
     * @return string
     */
    public function buildFooter(): string
    {
        return '</urlset>';
    }
}
