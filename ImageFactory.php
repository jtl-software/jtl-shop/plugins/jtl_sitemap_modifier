<?php declare(strict_types=1);
namespace Plugin\jtl_sitemap_modifier;

use Generator;
use JTL\Catalog\Product\Artikel;
use JTL\DB\ReturnType;
use JTL\Language\LanguageHelper;
use JTL\Sitemap\Factories\AbstractFactory;
use PDO;
use function Functional\first;
use function Functional\map;

/**
 * Class ImageFactory
 * @package Plugin\jtl_sitemap_modifier
 */
final class ImageFactory extends AbstractFactory
{
    /**
     * @inheritdoc
     */
    public function getCollection(array $languages, array $customerGroups): Generator
    {
        $options                 = Artikel::getDefaultOptions();
        $defaultCustomerGroupID  = first($customerGroups);
        $defaultLang             = LanguageHelper::getDefaultLanguage();
        $defaultLangID           = (int)$defaultLang->kSprache;
        $_SESSION['kSprache']    = $defaultLangID;
        $_SESSION['cISOSprache'] = $defaultLang->cISO;
        $languageIDs             = map($languages, static function ($e) {
            return (int)$e->kSprache;
        });
        $res                     = $this->db->queryPrepared(
            "SELECT tartikel.kArtikel AS id, tseo.kSprache AS langID
                FROM tartikel
                JOIN tseo 
                    ON tseo.cKey = 'kArtikel'
                    AND tseo.kKey = tartikel.kArtikel
                    AND tseo.kSprache IN (" . \implode(',', $languageIDs) . ')
                LEFT JOIN tartikelsichtbarkeit 
                    ON tartikel.kArtikel = tartikelsichtbarkeit.kArtikel
                    AND tartikelsichtbarkeit.kKundengruppe = :cgid
                WHERE tartikelsichtbarkeit.kArtikel IS NULL
                    AND tartikel.kVaterArtikel = 0
                ORDER BY tartikel.kArtikel',
            ['cgid' => $defaultCustomerGroupID],
            ReturnType::QUERYSINGLE
        );

        while (($data = $res->fetch(PDO::FETCH_OBJ)) !== false) {
            $product = new Artikel();
            $product->fuelleArtikel((int)$data->id, $options, $defaultCustomerGroupID, (int)$data->langID);
            $item = new ImageSitemapItem($this->config, $this->baseURL, $this->baseImageURL);
            $item->generateData($product, $languages);
            yield $item;
        }
    }
}
